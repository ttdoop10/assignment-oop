public class Utility {
    public int n_f;
	/**
	 * Test whether a specific number is a prime number.
	 * 
	 * @param num
	 *            the number
	 * @return <code>true</code> if <code>num</code> is a prime number.
	 */
	public static boolean isPrime(int num) {
		if (num < 2) {
			return false;
		}
		for (int i = 2; i < num / 2; i++) {
			if (num % i == 0){
				return false;
			}
		}
		return true;
	}

	/**
	 * Test whether a specific number is a square number.
	 * 
	 * @param num
	 *            the number
	 * @return <code>true</code> if <code>num</code> is a square number.
	 */

	public static boolean isSquare(int num) {
		int x = (int) Math.sqrt(num);
		return x*x == num;
	}


	// Returns true if n is a Fibonacci Number, else false
	public static boolean isFibonacci(int n)
	{
		// n is Fibonacci if one of 5*n*n + 4 or 5*n*n - 4 or both
		// is a perfect square
		return isSquare(5*n*n + 4) ||
				isSquare(5*n*n - 4);
	}


    public static int findIndex(int n)
    {
        // if Fibonacci number is less than 2,
        // its index will be same as number
        if (n <= 1)
            return n;

        int a = 0, b = 1, c = 1;
        int res = 1;

        // iterate until generated fibonacci number
        // is less than given fibonacci number
        while (c < n)
        {
            c = a + b;

            // res keeps track of number of generated
            // fibonacci number

            res++;
            a = b;
            b = c;
        }
        return res;
    }

}
