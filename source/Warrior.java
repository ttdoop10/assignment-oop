public class Warrior extends Fighter {
    public Warrior(int baseHp, int wp) {
        super(baseHp, wp);
    }


    @Override
    public double getCombatScore() {
        if (Utility.isPrime(Battle.GROUND)) return getBaseHp() * 2;

        if (getWp() == 1) return getBaseHp();

        return getBaseHp()/10;
    }
}
