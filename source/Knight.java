public class Knight extends Fighter {
    public Knight(int baseHp, int wp) {
        super(baseHp, wp);
    }

    @Override
    public double getCombatScore() {
        if (Utility.isSquare(Battle.GROUND)) return getBaseHp() * 2;

        if (getWp() == 1) return getBaseHp();

        return getBaseHp()/10;
    }
}
