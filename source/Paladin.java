
public class Paladin extends Knight {

    public Paladin(int baseHp, int wp) {
        super(baseHp, wp);
    }

    @Override
    public double getCombatScore() {
        if (Utility.isFibonacci(getBaseHp()))
            return 1000 + Utility.findIndex(getBaseHp());

        return 3*getBaseHp();
    }
}
